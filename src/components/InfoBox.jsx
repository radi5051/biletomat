import React, { Component } from "react";

class InfoBox extends Component {
  state = {};
  render() {
    return (
      <div className="info-box">
        <h1>{this.props.title}</h1>
        <h3>{this.props.content}</h3>
      </div>
    );
  }
}

export default InfoBox;
