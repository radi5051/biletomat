import React, { Component } from "react";

class CardInfoBox extends Component {
  state = {};
  render() {
    const {
      isSemestral,
      isDiscounted,
      card,
      type,
      endDate,
      price
    } = this.props;
    return (
      <div className="CardInfo-box">
        <h1>Informacje o bilecie</h1>
        <p>
          <b>Rodzaj biletu: </b>
          {isSemestral ? "Semestralny " : "Miesięczny "}
          {isDiscounted ? "ulgowy" : "normalny"}
        </p>
        <p>
          <b>Linie: </b>
          {card.name}
        </p>
        <p>
          <b>Ważność: </b>
          {type.name}
        </p>
        {endDate !== null ? (
          <p>
            <b>Data zakończenia: </b>
            {endDate.toLocaleDateString()}
          </p>
        ) : null}
        <p>
          <b>Cena: </b>
          {price} zł
        </p>
      </div>
    );
  }
}

export default CardInfoBox;
