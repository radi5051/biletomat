import React, { Component } from "react";
import "../styles/index.scss";

class Clock extends Component {
  state = {
    time: new Date().toLocaleString()
  };

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: new Date().toLocaleString()
    });
  }
  render() {
    return <h1 className="clock">{this.state.time}</h1>;
  }
}

export default Clock;
