import React, { Component } from "react";
import Clock from "./Clock";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Step from "./Step";
import { withRouter } from "react-router-dom";

class Header extends Component {
  state = {};

  handleBack = () => {
    this.props.history.goBack();
  };

  renderBackButton = () => {
    if (this.props.step > 0)
      return (
        <button className="button-icon m-2" onClick={this.handleBack}>
          <ArrowBackIcon />
        </button>
      );
    else return null;
  };

  render() {
    return (
      <div className="d-flex flex-row align-items-center justify-content-between p-2">
        <div className="p-2 w-25">
          <img width="70px" height="70px" src="z.png"></img>
        </div>
        <div className="p-2 w-50">
          <Clock />
        </div>
        <div className="p-2 d-flex flex-row w-25">
          {this.renderBackButton()}
          <Step step={this.props.step} maxStep={this.props.maxStep} />
        </div>
      </div>
    );
  }
}

export default withRouter(Header);
