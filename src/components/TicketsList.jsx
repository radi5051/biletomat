import React, { Component } from "react";
import { Link } from "react-router-dom";
import Pagination from "./Pagination";
import { paginate } from "./../utils/paginate";

class TicketsList extends Component {
  state = {
    currentPage: 1,
    pageSize: 4
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  renderChosenTickets = () => {
    const pagedTickets = paginate(
      this.props.chosenTickets,
      this.state.currentPage,
      this.state.pageSize
    );
    return pagedTickets.map(t => (
      <tr key={t.price}>
        <th scope="row">{t.name}</th>
        <td>{t.range}</td>
        <td>{t.count}</td>
        <td>{t.type}</td>
        <td>{t.price} zł</td>
      </tr>
    ));
  };

  renderSummary = () => {
    return (
      <p>
        <b>Podsumowanie: {this.props.price} zł </b>
      </p>
    );
  };

  render() {
    return (
      <div className="ticketList p-3">
        <table className="table table-striped table-sm bg-light text-center">
          <thead>
            <tr>
              <th scope="col">Bilet</th>
              <th scope="col">Zakres</th>
              <th scope="col">Ilość</th>
              <th scope="col">Typ</th>
              <th scope="col">Cena</th>
            </tr>
          </thead>
          <tbody>{this.renderChosenTickets()}</tbody>
        </table>
        {this.renderSummary()}
        <div className="d-flex flex-column align-items-center">
          <Pagination
            itemsCount={this.props.chosenTickets.length}
            pageSize={this.state.pageSize}
            currentPage={this.state.currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>

        <div className="text-center">
          <Link to="/ticketPayment">
            <button className="button-small">ZAPŁAĆ</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default TicketsList;
