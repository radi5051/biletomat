import React, { Component } from "react";

class PayBox extends Component {
  state = {};
  render() {
    return (
      <div className="payment-box d-flex flex-column justify-content-center ">
        <div>
          <h5>{this.props.title}</h5>
        </div>

        <div>
          <h1>{this.props.price} zł</h1>
        </div>
      </div>
    );
  }
}

export default PayBox;
