import React, { Component } from "react";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";

class Ticket extends Component {
  state = {};

  renderDeleteButton1 = () => {
    const { ticket } = this.props;
    if (
      (ticket.normalCount > 0 && !this.props.isConcessionary) ||
      (ticket.concessionaryCount > 0 && this.props.isConcessionary)
    ) {
      return (
        <button
          className="button-icon delete m-1"
          onClick={() =>
            this.props.onPaperTicketsUpdate(
              this.props.isMetropolitan,
              this.props.isConcessionary,
              this.props.ticket.id,
              -1
            )
          }
        >
          <RemoveIcon />
        </button>
      );
    }
  };
  renderDeleteButton5 = () => {
    const { ticket } = this.props;
    if (
      (ticket.normalCount > 1 && !this.props.isConcessionary) ||
      (ticket.concessionaryCount > 1 && this.props.isConcessionary)
    ) {
      return (
        <button
          className="button-icon delete m-1"
          onClick={() =>
            this.props.onPaperTicketsUpdate(
              this.props.isMetropolitan,
              this.props.isConcessionary,
              this.props.ticket.id,
              -5
            )
          }
        >
          <h4>-5</h4>
        </button>
      );
    }
  };

  render() {
    const { ticket } = this.props;
    return (
      <div className="d-flex flex-row ticket">
        <div className="d-flex flex-column mr-auto">
          <h5>{ticket.name}</h5>
          <p>
            {ticket.description}
            <br />
            <b>
              Cena:{" "}
              {this.props.isConcessionary
                ? ticket.basePrice / 2
                : ticket.basePrice}{" "}
              zł
            </b>
          </p>
        </div>
        <div className="d-flex flex-column">
          <div className="d-flex flex-row">
            <button
              className="button-icon add m-1"
              onClick={() =>
                this.props.onPaperTicketsUpdate(
                  this.props.isMetropolitan,
                  this.props.isConcessionary,
                  ticket.id,
                  1
                )
              }
            >
              <AddIcon />
            </button>
            <button
              className="button-icon add m-1"
              onClick={() =>
                this.props.onPaperTicketsUpdate(
                  this.props.isMetropolitan,
                  this.props.isConcessionary,
                  ticket.id,
                  5
                )
              }
            >
              <h4>+5</h4>
            </button>
          </div>
          <div className="d-flex flex-row">
            {this.renderDeleteButton1()}
            {this.renderDeleteButton5()}
          </div>
        </div>
      </div>
    );
  }
}

export default Ticket;
