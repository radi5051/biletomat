import React, { Component } from "react";
import { Link } from "react-router-dom";

const step = 0;

class Main extends Component {
  state = {};

  componentDidMount() {
    this.props.onSetStep(step);
  }

  render() {
    return (
      <div className="d-flex flex-column align-items-center">
        <div className="p-2">
          <Link to="/ticketChoice">
            <button className="button-large m-2">BILET</button>
          </Link>
        </div>
        <div className="p-2">
          <Link to="/cardImplementation">
            <button className="button-large m-2">KARTA MIEJSKA</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Main;
