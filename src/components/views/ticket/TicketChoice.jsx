import React, { Component } from "react";
import TicketsList from "../../TicketsList";
import Ticket from "../../Ticket";
import { Select, MenuItem, FormControl, InputLabel } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import { paginate } from "./../../../utils/paginate";

const step = 1;
const maxStep = 3;

class TicketChoice extends Component {
  state = {
    isMetropolitan: false,
    isConcessionary: false,
    currentPage: 1,
    pageSize: 2,
    pagesCount: 0
  };

  componentDidMount() {
    this.props.onSetStep(step);
    this.props.onSetMaxStep(maxStep);
    const pagesCount = Math.ceil(
      this.props.normalTickets.length / this.state.pageSize
    );
    this.setState({ pagesCount: pagesCount });
  }

  handlePriceUpdate = () => {
    let finalPrice = 0;
    for (let ticket of this.state.tickets) {
      if (ticket.count > 0) {
        finalPrice = finalPrice + ticket.count * ticket.price;
      }
    }
    this.setState({ price: finalPrice });
  };

  handleNormal = () => {
    this.setState({ isMetropolitan: false });
  };

  handleMetropolitan = () => {
    this.setState({ isMetropolitan: true });
  };

  handleConcessionaryChange = event => {
    this.setState({ isConcessionary: event.target.value });
  };

  handlePageUp = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
  };

  handlePageDown = () => {
    this.setState({ currentPage: this.state.currentPage - 1 });
  };

  handleIncrement = id => {
    let tickets = [...this.state.tickets];
    const index = tickets.findIndex(t => t.id === id);
    tickets[index].count++;

    this.setState({ tickets: tickets });
    this.handlePriceUpdate();
  };

  handleDecrement = id => {
    let tickets = [...this.state.tickets];
    const index = tickets.findIndex(t => t.id === id);
    tickets[index].count--;

    this.setState({ tickets: tickets });
    this.handlePriceUpdate();
  };

  renderPreviousPageButton = () => {
    if (this.state.currentPage > 1) {
      return (
        <button className="button-icon page m-1" onClick={this.handlePageDown}>
          <ExpandLessIcon />
        </button>
      );
    } else {
      return (
        <button
          className="button-icon page hidden m-1"
          onClick={this.handlePageDown}
        >
          <ExpandLessIcon />
        </button>
      );
    }
  };

  renderNextPageButton = () => {
    if (this.state.currentPage < this.state.pagesCount) {
      return (
        <button className="button-icon page m-1" onClick={this.handlePageUp}>
          <ExpandMoreIcon />
        </button>
      );
    } else {
      return (
        <button
          className="button-icon page hidden m-1"
          onClick={this.handlePageUp}
        >
          <ExpandMoreIcon />
        </button>
      );
    }
  };

  render() {
    let chosenTickets = [];
    if (this.state.isMetropolitan === true) {
      chosenTickets = this.props.metropolitanTickets;
    } else {
      chosenTickets = this.props.normalTickets;
    }
    chosenTickets = paginate(
      chosenTickets,
      this.state.currentPage,
      this.state.pageSize
    );
    return (
      <div className="d-flex flex-row justify-content-around">
        <div className="d-flex flex-column w-55">
          <div className="d-flex flex-row justify-content-around ">
            <button className="button-medium m-2" onClick={this.handleNormal}>
              ZWYKŁY
            </button>
            <button
              className="button-medium m-2"
              onClick={this.handleMetropolitan}
            >
              METROPOLITARNY
            </button>
          </div>
          <FormControl
            className="m-2 p-2 forms"
            size="medium"
            color="secondary"
          >
            <InputLabel className="p-2">Typ biletu</InputLabel>
            <Select
              labelId="card"
              // id="demo-controlled-open-select"
              onChange={this.handleConcessionaryChange}
              defaultValue={false}
            >
              <MenuItem value={false}>Normalny</MenuItem>
              <MenuItem value={true}>Ulgowy</MenuItem>
            </Select>
          </FormControl>
          <div className="d-flex flex-row">
            <div className="d-flex flex-column justify-content-between">
              {this.renderPreviousPageButton()}
              {this.renderNextPageButton()}
            </div>
            <div className="d-flex flex-column">
              <div className="d-flex flex-row flex-wrap justify-content-around">
                {chosenTickets.map(t => (
                  <Ticket
                    key={t.id}
                    ticket={t}
                    isConcessionary={this.state.isConcessionary}
                    isMetropolitan={this.state.isMetropolitan}
                    onPaperTicketsUpdate={this.props.onPaperTicketsUpdate}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex flex-column w-45">
          <TicketsList
            chosenTickets={this.props.chosenTickets}
            price={this.props.price}
          />
        </div>
      </div>
    );
  }
}

export default TicketChoice;
