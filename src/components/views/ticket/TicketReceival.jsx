import React, { Component } from "react";
import InfoBox from "./../../InfoBox";

// const step = 3;
// const maxStep = 3;
const SPACE_KEY = 32;

class TicketReveival extends Component {
  state = {
    step: 3,
    maxStep: 3
  };

  componentDidMount() {
    document.addEventListener("keydown", this._handleKeyDown);
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this._handleKeyDown);
  }

  _handleKeyDown = event => {
    if (event.keyCode === SPACE_KEY) {
      this.props.history.push("/home");
    }
  };

  render() {
    return (
      <div className="d-flex flex-column justify-content-center align-items-center">
        <InfoBox
          title="Odbierz resztę oraz bilet"
          content="Dziękujemy za skorzystanie z naszych usług :)"
        />
      </div>
    );
  }
}

export default TicketReveival;
