import React, { Component } from "react";
import InfoBox from "./../../InfoBox";

const SPACE_KEY = 32;

class CardImplementation extends Component {
  state = {
    step: 1,
    maxStep: 5
  };

  componentDidMount() {
    document.addEventListener("keydown", this._handleKeyDown);
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this._handleKeyDown);
  }

  _handleKeyDown = event => {
    if (event.keyCode === SPACE_KEY) {
      this.props.history.push("/cardInfo");
    }
  };

  render() {
    return (
      <div className="d-flex flex-column">
        <InfoBox title="Włóż kartę miejską do czytnika" />
        <img
          className="gif-box"
          src="card_implementation.gif"
          alt="Insert card..."
        />
      </div>
    );
  }
}

export default CardImplementation;
