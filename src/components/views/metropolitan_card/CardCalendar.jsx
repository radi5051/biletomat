import React, { Component } from "react";
import Calendar from "react-calendar";
import CardInfoBox from "../../CardInfoBox";
import { Link } from "react-router-dom";
import "../../../styles/override.scss";

class CalendarView extends Component {
  state = {
    step: 3,
    maxStep: 5,
    date: new Date()
  };

  componentDidMount() {
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
  }

  handleChange = date => {
    this.setState({ date });
  };

  render() {
    const {
      isSemestral,
      isDiscounted,
      card,
      type,
      endDate,
      price
    } = this.props.currentCard;
    return (
      <div className="d-flex flex-row justify-content-center">
        <div className="d-flex flex-column align-items-center w-50">
          <Calendar
            className="p-3 m-3 calendar"
            tileClassName="tile"
            onChange={this.handleChange}
            value={this.state.date}
            minDate={new Date()}
          />
        </div>

        <div className="d-flex flex-column align-items-center">
          <CardInfoBox
            isSemestral={isSemestral}
            isDiscounted={isDiscounted}
            card={card}
            type={type}
            endDate={endDate}
            price={price}
          />
          <Link to={`/cardPayment/${price}`}>
            <button
              className="button-medium m-2"
              //onClick={() => this.props.onSetPrice(this.state.card.price)}
            >
              ZAPŁAĆ
            </button>
          </Link>
        </div>
      </div>
    );
  }
}

export default CalendarView;
