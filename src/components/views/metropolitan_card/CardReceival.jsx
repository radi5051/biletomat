import React, { Component } from "react";
import InfoBox from "../../InfoBox";

const SPACE_KEY = 32;

class CardReceival extends Component {
  state = {
    step: 5,
    maxStep: 5
  };

  componentDidMount() {
    document.addEventListener("keydown", this._handleKeyDown);
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this._handleKeyDown);
  }

  _handleKeyDown = event => {
    if (event.keyCode === SPACE_KEY) {
      this.props.history.push("/home");
    }
  };

  render() {
    return (
      <div className="d-flex flex-column h-100 flex-grow align-items-center justify-content-center">
        <div className="">
          <InfoBox
            title="Odbierz resztę, a następnie kartę"
            content="Dziękujemy za skorzystanie z naszych usług :)"
          />
        </div>
      </div>
    );
  }
}

export default CardReceival;
