import React, { Component } from "react";
import CardInfoBox from "../../CardInfoBox";
import { Link } from "react-router-dom";

class CardInfo extends Component {
  state = {
    step: 2,
    maxStep: 5,
    card: {
      type: "Miesięczny ulgowy",
      lines: "nocne, pośpieszne i zwykłe w granicach Gdyni",
      validity: "7 grudnia 2019",
      price: "47 zł"
    }
  };

  componentDidMount() {
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
  }

  render() {
    const {
      isSemestral,
      isDiscounted,
      card,
      type,
      endDate,
      price
    } = this.props.currentCard;
    return (
      <div className="d-flex flex-column">
        <CardInfoBox
          isSemestral={isSemestral}
          isDiscounted={isDiscounted}
          card={card}
          type={type}
          endDate={endDate}
          price={price}
        />
        <div className="d-flex flex-row justify-content-center">
          <Link to="/cardCalendarRenew">
            <button className="button-medium m-2">ODNÓW BILET</button>
          </Link>

          <Link to="/cardChoice">
            <button className="button-medium m-2">NOWY BILET</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default CardInfo;
