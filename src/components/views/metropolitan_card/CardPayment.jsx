import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import LinearProgress from "@material-ui/core/LinearProgress";
import PayBox from "../../PayBox";
import InfoBox from "../../InfoBox";

const ONE_KEY = 49;
const TWO_KEY = 50;
const FIVE_KEY = 53;
const SPACE_KEY = 32;

class CardPayment extends Component {
  state = {
    step: 4,
    maxStep: 5,
    isDialogVisible: false,
    givenMoney: 0
  };

  componentDidMount() {
    document.addEventListener("keydown", this._handleKeyDown);
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this._handleKeyDown);
  }

  _handleKeyDown = event => {
    switch (event.keyCode) {
      case SPACE_KEY:
        this.setState({
          givenMoney: parseFloat(this.props.match.params.price)
        });
        break;
      case ONE_KEY:
        this.setState({ givenMoney: this.state.givenMoney + 1 });
        break;
      case TWO_KEY:
        this.setState({ givenMoney: this.state.givenMoney + 2 });
        break;
      case FIVE_KEY:
        this.setState({ givenMoney: this.state.givenMoney + 5 });
        break;
      default:
        break;
    }
    if (this.state.givenMoney >= this.props.match.params.price)
      this.setState({ isDialogVisible: true });
  };

  handlePrinting = () => {
    this.timerID = setTimeout(() => this.handleClose(), 5000);
  };

  handleClose = () => {
    this.setState({ isDialogVisible: false });
    this.props.history.push("/cardReceival");
  };

  render() {
    return (
      <div
        className="d-flex flex-column align-items-center"
        onKeyPress={this.handleKeyPress}
      >
        <InfoBox title="ZAPŁAĆ KARTĄ LUB GOTÓWKĄ" />
        <div className="d-flex justify-content-around">
          <div className="m-3">
            <PayBox
              title="Zapłacono"
              price={this.state.givenMoney.toFixed(2)}
            />
          </div>
          <div className="m-3">
            <PayBox
              title="Suma do zapłaty"
              //price={this.props.price.toFixed(2)}
              price={parseFloat(this.props.match.params.price).toFixed(2)}
            />
          </div>
        </div>
        <Dialog
          onClose={this.handleClose}
          aria-labelledby="customized-dialog-title"
          open={this.state.isDialogVisible}
          onEnter={this.handlePrinting}
        >
          <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
            Karta Miejska
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Doładowywanie karty miejskiej ... Proszę czekać
            </DialogContentText>
            <LinearProgress />
          </DialogContent>
          <DialogActions></DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default CardPayment;
