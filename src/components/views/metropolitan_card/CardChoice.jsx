import React, { Component } from "react";
import { Select, MenuItem, FormControl, InputLabel } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import Card from "../../Card";
import { paginate } from "./../../../utils/paginate";

class CardChoice extends Component {
  state = {
    step: 3,
    maxStep: 5,
    isSemestral: false,
    isConcessionary: false,
    selectId: 1,
    semestralCurrentPage: 1,
    monthlyCurrentPage: 1,
    pageSize: 2,
    semestralPagesCount: 0,
    monthlyPagesCount: 0
  };

  componentDidMount() {
    this.props.onSetStep(this.state.step);
    this.props.onSetMaxStep(this.state.maxStep);
    let pagesCount = 0;
    pagesCount = Math.ceil(
      this.props.semestralTickets.length / this.state.pageSize
    );
    this.setState({ semestralPagesCount: pagesCount });

    pagesCount = Math.ceil(
      this.props.monthlyTickets.length / this.state.pageSize
    );
    this.setState({ monthlyPagesCount: pagesCount });
  }

  handleChange = event => {
    this.setState({ selectId: event.target.value });
  };

  handleSemestral = () => {
    this.setState({ isSemestral: true });
  };

  handleMonthly = () => {
    this.setState({ isSemestral: false });
  };

  handleConcessionaryChange = event => {
    this.setState({ isConcessionary: event.target.value });
  };

  handleSemestralPageUp = () => {
    this.setState({
      semestralCurrentPage: this.state.semestralCurrentPage + 1
    });
  };

  handleSemestralPageDown = () => {
    this.setState({
      semestralCurrentPage: this.state.semestralCurrentPage - 1
    });
  };

  handleMonthlyPageUp = () => {
    this.setState({ monthlyCurrentPage: this.state.monthlyCurrentPage + 1 });
  };

  handleMonthlyPageDown = () => {
    this.setState({ monthlyCurrentPage: this.state.monthlyCurrentPage - 1 });
  };

  renderTypeSelector = () => {
    return (
      <FormControl className="m-2" size="medium" color="secondary">
        <InputLabel>Typ doładowania</InputLabel>
        <Select labelId="card" onChange={this.handleChange} defaultValue={1}>
          {this.renderTypeSelectorChoices()}
        </Select>
      </FormControl>
    );
  };

  renderTypeSelectorChoices = () => {
    let choicesToRender;
    if (this.state.isSemestral === true) {
      choicesToRender = this.props.cardTypes.filter(
        c => c.isSemestral === true
      );
    } else {
      choicesToRender = this.props.cardTypes.filter(
        c => c.isSemestral === false
      );
    }
    this.setState();
    return choicesToRender.map((choice, index) => (
      <MenuItem selected value={index + 1}>
        {choice.name}
      </MenuItem>
    ));
  };

  renderChoices = () => {
    if (this.state.isSemestral === true) {
      const semestralTicketsToRender = paginate(
        this.props.semestralTickets,
        this.state.semestralCurrentPage,
        this.state.pageSize
      );
      const selectedType = this.props.cardTypes[this.state.selectId - 1];
      return semestralTicketsToRender.map(t =>
        this._renderSemestralCard(t, selectedType)
      );
    } else {
      const monthlyTicketsToRender = paginate(
        this.props.monthlyTickets,
        this.state.monthlyCurrentPage,
        this.state.pageSize
      );
      const selectedType = this.props.cardTypes[this.state.selectId + 1];
      return monthlyTicketsToRender.map(t =>
        this._renderMonthlyCard(t, selectedType)
      );
    }
  };

  _renderSemestralCard = (t, selectedType) => {
    const isPriceLower = selectedType.lowerPrice;
    return (
      <Card
        className="p-2"
        key={t.id}
        id={t.id}
        name={t.name}
        price={isPriceLower ? t.lowerPrice : t.higherPrice}
        onSetCurrentCard={this.props.onSetCurrentCard}
        cardInfo={{
          isSemestral: this.state.isSemestral,
          isDiscounted: this.state.isConcessionary,
          card: t,
          type: selectedType
        }}
      />
    );
  };

  _renderMonthlyCard = (t, selectedType) => {
    const isPriceLower = selectedType.lowerPrice;
    let discount = 0;
    if (this.state.isConcessionary) {
      discount = 0.5;
    } else {
      discount = 1;
    }
    return (
      <Card
        className="p-2"
        key={t.id}
        id={t.id}
        name={t.name}
        price={
          isPriceLower ? t.lowerPrice * discount : t.higherPrice * discount
        }
        onSetCurrentCard={this.props.onSetCurrentCard}
        cardInfo={{
          isSemestral: this.state.isSemestral,
          isDiscounted: this.state.isConcessionary,
          card: t,
          type: selectedType
        }}
      />
    );
  };

  renderPreviousPageButton = () => {
    if (this.state.isSemestral) {
      if (this.state.semestralCurrentPage > 1) {
        return (
          <button
            className="button-icon page m-1"
            onClick={this.handleSemestralPageDown}
          >
            <ExpandLessIcon />
          </button>
        );
      } else {
        return (
          <button
            className="button-icon page hidden m-1"
            onClick={this.handleSemestralPageDown}
          >
            <ExpandLessIcon />
          </button>
        );
      }
    } else {
      if (this.state.monthlyCurrentPage > 1) {
        return (
          <button
            className="button-icon page m-1"
            onClick={this.handleMonthlyPageDown}
          >
            <ExpandLessIcon />
          </button>
        );
      } else {
        return (
          <button
            className="button-icon page hidden m-1"
            onClick={this.handleMonthlyPageDown}
          >
            <ExpandLessIcon />
          </button>
        );
      }
    }
  };

  renderNextPageButton = () => {
    if (this.state.isSemestral) {
      if (this.state.semestralCurrentPage < this.state.semestralPagesCount) {
        return (
          <button
            className="button-icon page m-1"
            onClick={this.handleSemestralPageUp}
          >
            <ExpandMoreIcon />
          </button>
        );
      } else {
        return (
          <button
            className="button-icon page hidden m-1"
            onClick={this.handleSemestralPageUp}
          >
            <ExpandMoreIcon />
          </button>
        );
      }
    } else {
      if (this.state.monthlyCurrentPage < this.state.monthlyPagesCount) {
        return (
          <button
            className="button-icon page m-1"
            onClick={this.handleMonthlyPageUp}
          >
            <ExpandMoreIcon />
          </button>
        );
      } else {
        return (
          <button
            className="button-icon page hidden m-1"
            onClick={this.handleMonthlyPageUp}
          >
            <ExpandMoreIcon />
          </button>
        );
      }
    }
  };

  render() {
    return (
      <div className="d-flex flex-row justify-content-around">
        <div className="d-flex flex-column">
          <div className="d-flex flex-row justify-content-around ">
            <button className="button-medium m-2" onClick={this.handleMonthly}>
              BILET MIESIĘCZNY
            </button>
            <button
              className="button-medium m-2"
              onClick={this.handleSemestral}
            >
              BILET SEMESTRALNY
            </button>
          </div>
          <div className="d-flex flex-row justify-content-around ">
            <FormControl
              className="m-2 p-2 w-55 forms"
              size="medium"
              color="secondary"
            >
              <InputLabel className="m-2 " color="secondary">
                Typ doładowania
              </InputLabel>
              <Select
                labelId="card"
                onChange={this.handleChange}
                defaultValue={1}
              >
                {this.renderTypeSelectorChoices()}
              </Select>
            </FormControl>
            <div className={this.state.isSemestral ? "w-25" : "dont-display"}>
              <FormControl
                className="m-2 p-2 w-100 forms"
                size="medium"
                color="secondary"
              >
                <InputLabel className="m-2 " color="secondary">
                  Typ biletu
                </InputLabel>
                <Select
                  labelId="card"
                  //onChange={this.handleConcessionaryChange}
                  defaultValue={true}
                >
                  <MenuItem value={true}>Ulgowy</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className={this.state.isSemestral ? "dont-display" : "w-25"}>
              <FormControl
                className="m-2 p-2 w-100 forms"
                size="medium"
                color="secondary"
              >
                <InputLabel className="m-2" color="secondary">
                  Typ biletu
                </InputLabel>
                <Select
                  labelId="card"
                  onChange={this.handleConcessionaryChange}
                  defaultValue={false}
                >
                  <MenuItem value={false}>Normalny</MenuItem>
                  <MenuItem value={true}>Ulgowy</MenuItem>
                </Select>
              </FormControl>
            </div>
          </div>

          <div className="d-flex flex-row flex-wrap justify-content-around">
            <div className="d-flex flex-column justify-content-between">
              {this.renderPreviousPageButton()}
              {this.renderNextPageButton()}
            </div>
            <div className="d-flex flex-column">{this.renderChoices()}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardChoice;
