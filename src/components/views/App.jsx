import React, { Component } from "react";
import "../../styles/index.scss";
import "../../styles/override.scss";
import "bootstrap/dist/css/bootstrap.css";
import { Route, Switch, Redirect } from "react-router-dom";
import Main from "./Main";
import TicketChoice from "./ticket/TicketChoice";
import TicketPayment from "./ticket/TicketPayment";
import Header from "../Header";
import CardImplementation from "./metropolitan_card/CardImplementation";
import TicketReveival from "./ticket/TicketReceival";
import CardInfo from "./metropolitan_card/CardInfo";
import CardCalendar from "./metropolitan_card/CardCalendar";
import CardPayment from "./metropolitan_card/CardPayment";
import CardReceival from "./metropolitan_card/CardReceival";
import CardChoice from "./metropolitan_card/CardChoice";

const semestralTickets = [
  {
    id: 1,
    name: "Zwykłe w granicach Gdyni",
    description: "",
    lowerPrice: 156,
    higherPrice: 195
  },
  {
    id: 2,
    name: "Nocne, pospieszne i zwykłe w granicach Gdyni",
    description: "",
    lowerPrice: 179,
    higherPrice: 223
  },
  {
    id: 3,
    name:
      "Nocne, pospieszne i zwykłe w granicach Sopotu albo Rumi albo Gm. Kosakowo albo Gm. Żukowo albo Gm. Szemud albo Gm. Wejherowo",
    description: "",
    lowerPrice: 122,
    higherPrice: 152
  },
  {
    id: 4,
    name:
      "Nocne, pospieszne i zwykłe w granicach Rumi, Redy i miasta Wejherowa albo Gm. Wejherowo i Rumi",
    description: "",
    lowerPrice: 160,
    higherPrice: 200
  },
  {
    id: 5,
    name:
      "Nocne, pospieszne i zwykłe w obrębie sieci komunikacyjnej [w tym linie G, N1, 101 i 171]",
    description: "",
    lowerPrice: 198,
    higherPrice: 247
  }
];

const monthlyTickets = [
  {
    id: 1,
    name: "Zwykłe w granicach Gdyni",
    description: "",
    lowerPrice: 72,
    higherPrice: 82
  },
  {
    id: 2,
    name: "Nocne, pospieszne i zwykłe w granicach Gdyni",
    description: "",
    lowerPrice: 86,
    higherPrice: 94
  },
  {
    id: 3,
    name:
      "Nocne, pospieszne i zwykłe w granicach Sopotu albo Rumi albo Gm. Kosakowo albo Gm. Żukowo albo Gm. Szemud albo Gm. Wejherowo",
    description: "",
    lowerPrice: 58,
    higherPrice: 64
  },
  {
    id: 4,
    name:
      "Nocne, pospieszne i zwykłe w granicach Rumi, Redy i miasta Wejherowa albo Gm. Wejherowo i Rumi",
    description: "",
    lowerPrice: 74,
    higherPrice: 84
  },
  {
    id: 5,
    name:
      "Nocne, pospieszne i zwykłe w obrębie sieci komunikacyjnej [w tym linie G, N1, 101 i 171]",
    description: "",
    lowerPrice: 96,
    higherPrice: 104
  }
];

const cardTypes = [
  {
    id: 1,
    name:
      "4-miesięczny ważny we wszystkie dni tygodnia 01.10-31.01 lub 01.02-31.05",
    isSemestral: true,
    lowerPrice: true
  },
  {
    id: 2,
    name:
      "5-miesięczny ważny we wszystkie dni tygodnia 01.09-31.01 lub 01.02-30.06",
    isSemestral: true,
    lowerPrice: false
  },
  {
    id: 3,
    name: "ważny od poniedziałku do piątku",
    isSemestral: false,
    lowerPrice: true
  },
  {
    id: 4,
    name: "ważny we wszystkie dni tygodnia",
    isSemestral: false,
    lowerPrice: false
  }
];

class App extends Component {
  state = {
    step: 0,
    maxStep: 0,
    price: 0,
    loadedCard: {
      isSemestral: false,
      isDiscounted: true,
      card: monthlyTickets[0],
      type: cardTypes[3],
      endDate: new Date(),
      price: 36
    },
    currentCard: {
      isSemestral: false,
      isDiscounted: true,
      card: monthlyTickets[0],
      type: cardTypes[3],
      endDate: new Date(),
      price: 36
    },

    isMetropolitan: false,
    normalTickets: [
      {
        id: 1,
        name: "Jednoprzejazdowy",
        description: "na linie zwykłe",
        basePrice: 3.2,
        normalCount: 0,
        concessionaryCount: 0
      },
      {
        id: 2,
        name: "1-godzinny",
        description: "na linie zwykłe",
        basePrice: 3.8,
        normalCount: 0,
        concessionaryCount: 0
      },
      {
        id: 3,
        name: "1-godzinny lub jednoprzejazdowy",
        description: "na linie nocne, pośpieszne i zwykłe",
        basePrice: 4.2,
        normalCount: 0,
        concessionaryCount: 0
      },
      {
        id: 4,
        name: "24-godzinny",
        description: "na linie nocne, pośpieszne i zwykłe",
        basePrice: 13,
        normalCount: 0,
        concessionaryCount: 0
      }
    ],
    metropolitanTickets: [
      {
        id: 1,
        name: "Jednoprzejazdowy",
        description:
          "na linie zwykłe - obowiązujący w pojazdach: ZTM w Gdańsku, ZKM w Gdyni i MZK Wejherowo",
        basePrice: 3.6,
        normalCount: 0,
        concessionaryCount: 0
      },
      {
        id: 2,
        name: "Jednoprzejazdowy",
        description:
          "na linie zwykłe, pośpieszne i nocne - obowiązujący w pojazdach: ZTM w Gdańsku, ZKM w Gdyni i MZK Wejherowo",
        basePrice: 4.4,
        normalCount: 0,
        concessionaryCount: 0
      },
      {
        id: 3,
        name: "24 - godzinny komunalny",
        description:
          "komunalny obowiązujący w pojazdach: ZTM w Gdańsku, ZKM w Gdyni i MZK Wejherowo",
        basePrice: 15,
        normalCount: 0,
        concessionaryCount: 0
      },
      {
        id: 4,
        name: "24 - godzinny kolejowo - komunalny",
        description:
          "kolejowo - komunalny dwóch organizatorów, obowiązuje w: pociągi SKM i PR, pojazdy: ZTM Gdańsk, ZKM Gdynia i MZK Wejherowo",
        basePrice: 20,
        normalCount: 0,
        concessionaryCount: 0
      }
    ],
    chosenTickets: []
  };

  handlePaperTicketsUpdate = (isMetropolitan, isConcessionary, id, value) => {
    let tickets = [];
    if (isMetropolitan) {
      tickets = [...this.state.metropolitanTickets];
      const index = tickets.findIndex(t => t.id === id);
      if (isConcessionary) {
        tickets[index].concessionaryCount =
          tickets[index].concessionaryCount + value;
        if (tickets[index].concessionaryCount < 0)
          tickets[index].concessionaryCount = 0;
      } else {
        tickets[index].normalCount = tickets[index].normalCount + value;
        if (tickets[index].normalCount < 0) tickets[index].normalCount = 0;
      }
      this.setState({ metropolitanTickets: tickets });
    } else {
      tickets = [...this.state.normalTickets];
      const index = tickets.findIndex(t => t.id === id);
      if (isConcessionary) {
        tickets[index].concessionaryCount =
          tickets[index].concessionaryCount + value;
        if (tickets[index].concessionaryCount < 0)
          tickets[index].concessionaryCount = 0;
      } else {
        tickets[index].normalCount = tickets[index].normalCount + value;
        if (tickets[index].normalCount < 0) tickets[index].normalCount = 0;
      }
      this.setState({ normalTickets: tickets });
    }

    let chosenTickets = [];

    const normalNotConcessionaryTicketsToRender = this.state.normalTickets.filter(
      t => t.normalCount > 0
    );

    normalNotConcessionaryTicketsToRender.forEach(ticket => {
      chosenTickets.push({
        name: ticket.name,
        range: "Zwykły (Gdynia)",
        count: ticket.normalCount,
        type: "N",
        price: (ticket.normalCount * ticket.basePrice).toFixed(2)
      });
    });

    const normalConcessionaryTicketsToRender = this.state.normalTickets.filter(
      t => t.concessionaryCount > 0
    );

    normalConcessionaryTicketsToRender.forEach(ticket => {
      chosenTickets.push({
        name: ticket.name,
        range: "Zwykły (Gdynia)",
        count: ticket.concessionaryCount,
        type: "U",
        price: (ticket.concessionaryCount * ticket.basePrice * 0.5).toFixed(2)
      });
    });

    const metropolitanNotConcessionaryTicketsToRender = this.state.metropolitanTickets.filter(
      t => t.normalCount > 0
    );

    metropolitanNotConcessionaryTicketsToRender.forEach(ticket => {
      chosenTickets.push({
        name: ticket.name,
        range: "Metropolitalny",
        count: ticket.normalCount,
        type: "N",
        price: (ticket.normalCount * ticket.basePrice).toFixed(2)
      });
    });

    const metropolitanConcessionaryTicketsToRender = this.state.metropolitanTickets.filter(
      t => t.concessionaryCount > 0
    );

    metropolitanConcessionaryTicketsToRender.forEach(ticket => {
      chosenTickets.push({
        name: ticket.name,
        range: "Metropolitalny",
        count: ticket.concessionaryCount,
        type: "U",
        price: (ticket.concessionaryCount * ticket.basePrice * 0.5).toFixed(2)
      });
    });

    this.setState({ chosenTickets: chosenTickets });
    let sum = 0;
    chosenTickets.forEach(t => (sum = sum + parseFloat(t.price)));
    sum = sum.toFixed(2);
    this.setState({ price: sum });
  };

  handleSetStep = step => {
    this.setState({ step: step });
  };

  handleSetMaxStep = maxStep => {
    this.setState({ maxStep: maxStep });
  };

  handleSetPrice = price => {
    this.setState({ price: price });
  };

  handleSetCurrentCard = (isSemestral, isDiscounted, card, type, price) => {
    this.setState({
      currentCard: {
        isSemestral: isSemestral,
        isDiscounted: isDiscounted,
        card: card,
        type: type,
        endDate: null,
        price: price
      }
    });
  };

  render() {
    return (
      <div className="container">
        <Header step={this.state.step} maxStep={this.state.maxStep} />
        <Switch>
          <Route
            path="/home"
            render={() => (
              <Main
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
              />
            )}
          />
          <Route
            path="/ticketChoice"
            render={() => (
              <TicketChoice
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                onPaperTicketsUpdate={this.handlePaperTicketsUpdate}
                metropolitanTickets={this.state.metropolitanTickets}
                normalTickets={this.state.normalTickets}
                chosenTickets={this.state.chosenTickets}
                price={this.state.price}
              />
            )}
          />
          <Route
            path="/ticketPayment"
            render={props => (
              <TicketPayment
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                price={this.state.price}
                {...props}
              />
            )}
          />
          <Route
            path="/ticketReceival"
            render={props => (
              <TicketReveival
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                {...props}
              />
            )}
          />
          <Route
            path="/cardImplementation"
            render={props => (
              <CardImplementation
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                {...props}
              />
            )}
          />
          <Route
            path="/cardInfo"
            render={() => (
              <CardInfo
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                currentCard={this.state.loadedCard}
              />
            )}
          />
          <Route
            path="/cardCalendarRenew"
            render={() => (
              <CardCalendar
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                onSetPrice={this.handleSetPrice}
                currentCard={this.state.loadedCard}
                // card={this.filterCards()}
              />
            )}
          />
          <Route
            path="/cardCalendarNew"
            render={() => (
              <CardCalendar
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                onSetPrice={this.handleSetPrice}
                currentCard={this.state.currentCard}
                // card={this.filterCards()}
              />
            )}
          />
          <Route
            path="/cardPayment/:price"
            render={props => (
              <CardPayment
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                price={this.state.price}
                {...props}
              />
            )}
          />
          <Route
            path="/cardReceival"
            render={props => (
              <CardReceival
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                {...props}
              />
            )}
          />
          <Route
            path="/cardChoice"
            render={() => (
              <CardChoice
                onSetStep={this.handleSetStep}
                onSetMaxStep={this.handleSetMaxStep}
                onSetPrice={this.handleSetPrice}
                onSetCurrentCard={this.handleSetCurrentCard}
                onSetIsSemestral={this.handleSetIsSemestral}
                semestralTickets={semestralTickets}
                monthlyTickets={monthlyTickets}
                cardTypes={cardTypes}
              />
            )}
          />
          <Redirect to="/home" />
        </Switch>
      </div>
    );
  }
}

export default App;
