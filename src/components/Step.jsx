import React, { Component } from "react";

class Step extends Component {
  renderStep = () => {
    if (this.props.step > 0)
      return (
        <h2>
          Krok {this.props.step} / {this.props.maxStep}
        </h2>
      );
    else return null;
  };

  render() {
    return (
      <div className={this.props.step > 0 ? "step" : ""}>
        {this.renderStep()}
      </div>
    );
  }
}

export default Step;
