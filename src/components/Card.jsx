import React, { Component } from "react";
import { Link } from "react-router-dom";

class Card extends Component {
  state = {};
  render() {
    const { cardInfo } = this.props;
    return (
      <div className="d-flex flex-column p-2 card-box">
        <h6>{this.props.name}</h6>
        <p>
          <b>Cena: {this.props.price} zł</b>
        </p>
        <Link to="/cardCalendarNew">
          <button
            className="button-vsmall"
            onClick={() =>
              this.props.onSetCurrentCard(
                cardInfo.isSemestral,
                cardInfo.isDiscounted,
                cardInfo.card,
                cardInfo.type,
                this.props.price
              )
            }
          >
            WYBIERZ
          </button>
        </Link>
      </div>
    );
  }
}

export default Card;
